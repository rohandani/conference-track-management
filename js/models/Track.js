/**
 * Created by Rohan on 4/30/2017.
 */
var Track = function(id, previousTrackDay, firstHalfMin, secondHalfMin) {
    var firstHalfMin = (typeof firstHalfMin == Number && firstHalfMin > 0)? firstHalfMin: 180 ;
    var secondHalfMin = (typeof secondHalfMin == Number && secondHalfMin > 0)? secondHalfMin: 240 ;
    var firstHalfStartTime, secondHalfStartTime;
    if(previousTrackDay) {
        firstHalfStartTime = moment(previousTrackDay).add(1, "days");
        firstHalfStartTime = firstHalfStartTime.set({hour:9,minute:0,second:0,millisecond:0});
        firstHalfStartTime = firstHalfStartTime.toDate();
        secondHalfStartTime = moment(previousTrackDay).add(1, "days");
        secondHalfStartTime = secondHalfStartTime.set({hour:13,minute:0,second:0,millisecond:0});
        secondHalfStartTime = secondHalfStartTime.toDate();

    }else {
        // Set to next week Monday
        firstHalfStartTime = moment().day(8).set({hour:9,minute:0,second:0,millisecond:0});
        firstHalfStartTime = firstHalfStartTime.toDate();
        secondHalfStartTime = moment().day(8).set({hour:13,minute:0,second:0,millisecond:0});
        secondHalfStartTime = secondHalfStartTime.toDate();
    }
    return {
        id: id,
        firstHalfMin: firstHalfMin,
        secondHalfMin: secondHalfMin,
        firstHalfRemainingMin: firstHalfMin,
        secondHalfRemainingMin: secondHalfMin,
        firstHalfStartTime: firstHalfStartTime.toString(),
        secondHalfStartTime: secondHalfStartTime.toString(),
        events: {firsthalf: [], secondhalf: []},

        updateRemainingTime: function(eventObj, whichHalf) {
            if(whichHalf == "firsthalf") {
                this.firstHalfRemainingMin -= eventObj.duration;
                this.firstHalfStartTime = moment(this.firstHalfStartTime).add(eventObj.duration, "minutes");
                this.firstHalfStartTime = this.firstHalfStartTime.toString();
            }else{
                this.secondHalfRemainingMin -= eventObj.duration;
                this.secondHalfStartTime = moment(this.secondHalfStartTime).add(eventObj.duration, "minutes");
                this.secondHalfStartTime = this.secondHalfStartTime.toString();
            }
            this.updateEvents(eventObj, whichHalf);
        },

        updateEvents: function(eventObj, whichHalf) {
            this.events[whichHalf].push(eventObj);
        }
    }
};