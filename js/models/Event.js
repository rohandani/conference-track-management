/**
 * Created by Rohan on 4/30/2017.
 */
var Event = function(name, duration, type) {
    var type = (typeof type === "string" && type.trim().length > 0)? type: "Normal" ;
    var duration = (typeof type === "string" && type.toLowerCase() == "lightning")? 5: duration ;
    duration = parseInt(duration, 10);
    return {
        name: name,
        duration: duration,
        type: type,
        state: "unscheduled",
        startTime: "",
        endTime: "",
        trackNo: "",
        whichHalf: "",
        schedule: function(trackObj, whichhalf) {
            this.state = "scheduled";
            this.trackNo = trackObj.id;
            this.whichHalf = whichhalf;
            this.startTime = trackObj.firstHalfStartTime;
            if(whichhalf === "secondhalf") {
                this.startTime = trackObj.secondHalfStartTime;
            }
            this.endTime = moment(this.startTime).add(this.duration, "minutes")
            this.endTime = this.endTime.toString();
        }
    }
};