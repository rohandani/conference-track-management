/**
 * Created by Rohan on 4/30/2017.
 */
var EventController = function() {
    var self = this, currentTrack, currentEvent;
    this.tracksColl = [];


    this.createEvent = function(eventObj) {
        currentEvent = new Event(eventObj.eventName, eventObj.eventDuration, eventObj.type);
        if(self.tracksColl.length > 0) { // Get latest track
            currentTrack = self.tracksColl[self.tracksColl.length-1];
        }else{ // create first track
            currentTrack = self.createNewTrack();
        }
        //alert(JSON.stringify(currentTrack))
        self.insertEvent();
    };

    this.insertEvent = function() {
        if(currentEvent.duration <= currentTrack.firstHalfRemainingMin) {
            // Insert event in first half
            currentEvent.schedule(currentTrack, "firsthalf");
            currentTrack.updateRemainingTime(currentEvent, "firsthalf");
        }else if(currentEvent.duration <= currentTrack.secondHalfRemainingMin) {
            // Insert event in second half
            currentEvent.schedule(currentTrack, "secondhalf");
            currentTrack.updateRemainingTime(currentEvent, "secondhalf");
        }else if(currentEvent.duration <= (currentTrack.firstHalfRemainingMin + currentTrack.secondHalfRemainingMin)){
            // Check if the event can be managed after reshuffling above one or more events
            self.checkEventSwapping();
        }else{
            currentTrack = self.createNewTrack();
            self.insertEvent();
        }
        alert("Event saved successfully");
        sessionStorage.setItem("currentEvent", JSON.stringify(currentEvent));
        sessionStorage.setItem("currentTrack", JSON.stringify(currentTrack));
    };

    this.checkEventSwapping = function() {
        var firstHalfEvents = currentTrack.events.firsthalf, secondHalfEvents = currentTrack.events.secondhalf,
            firstHalfSwapEvents = [], secondHalfSwapEvents = [], tempFirstHalfEmptySpace = 0, tempSecondHalfEmptySpace = 0;
        for(var i=0; i< firstHalfEvents.length; i++) {
            if(firstHalfEvents[i].duration <= currentTrack.secondHalfRemainingMin) {
                tempFirstHalfEmptySpace += firstHalfEvents[i].duration;
                if(tempFirstHalfEmptySpace <= currentTrack.secondHalfRemainingMin) {
                    firstHalfSwapEvents.push(firstHalfEvents[i]);
                }
            }
        }
        if(tempFirstHalfEmptySpace + currentTrack.firstHalfRemainingMin <= currentEvent.duration) {
            // By moving first half elements to second half new event can be adjusted in first half
            self.moveEventsToOtherHalf(firstHalfSwapEvents, "secondhalf", "firsthalf");
            return;
        }
        for(var j=0; j< secondHalfEvents.length; j++) {
            if(secondHalfEvents[j].duration <= currentTrack.firstHalfRemainingMin) {
                tempSecondHalfEmptySpace += secondHalfEvents[j].duration;
                if(tempSecondHalfEmptySpace <= currentTrack.firstHalfRemainingMin) {
                    secondHalfSwapEvents.push(secondHalfEvents[j]);
                }
            }
        }

        if(tempSecondHalfEmptySpace + currentTrack.secondHalfRemainingMin <= currentEvent.duration) {
            // By moving second half elements to first half new event can be adjusted in second half
            self.moveEventsToOtherHalf(secondHalfSwapEvents, "firsthalf", "secondhalf");
            return;
        }else{
            // Event can not be adjusted
            currentTrack = self.createNewTrack();
            self.insertEvent();
        }
    };

    this.moveEventsToOtherHalf = function(eventsArr, moveToWhichHalf, moveFromWhichHalf) {
        var deletionHalf = currentTrack.events[moveFromWhichHalf];
        for(var i=0; i< eventsArr.length; i++) {
            eventsArr[i].schedule(currentTrack, moveToWhichHalf);
            currentTrack.updateRemainingTime(eventsArr[i], moveToWhichHalf);
            for(var j=deletionHalf.length- 1; j--;) {
                if(deletionHalf[j].name == eventsArr[i].name) {
                    deletionHalf.splice(j, 1);
                }
            }
        }
        self.rescheduleAfterDeletion(deletionHalf, moveFromWhichHalf);
        self.insertEvent();
    };

    this.rescheduleAfterDeletion = function(arr, whichhalf) {
        if(whichhalf == "firsthalf") {
            currentTrack.firstHalfRemainingMin = currentTrack.firstHalfMin;
            currentTrack.firstHalfStartTime = moment(currentTrack.firstHalfStartTime).set({hour:9,minute:0,second:0,millisecond:0});
            currentTrack.firstHalfStartTime = currentTrack.firstHalfStartTime.toDate();
        }else{
            currentTrack.secondHalfRemainingMin = currentTrack.secondHalfMin;
            currentTrack.secondHalfStartTime = moment(currentTrack.secondHalfStartTime).set({hour:13,minute:0,second:0,millisecond:0});
            currentTrack.secondHalfStartTime = currentTrack.secondHalfStartTime.toDate();
        }
        currentTrack.events[whichhalf] = [];
        for(var i=0; i< arr.length; i++) {
            arr[i].schedule(currentTrack, whichhalf);
            currentTrack.updateRemainingTime(arr[i], whichhalf);
        }
    };

    this.createNewTrack = function() {
        var previousTrackDay = "";
        if(self.tracksColl.length > 0) {
            previousTrackDay = self.tracksColl[self.tracksColl.length-1].firstHalfStartTime;
        }
        var newTrack = new Track(self.tracksColl.length + 1, previousTrackDay);
        self.tracksColl.push(newTrack);
        return newTrack
    };

    this.showEvents = function() {
        var source = $("#tracks-template").html();
        var template = Handlebars.compile(source);
        var html = template(self.tracksColl);
        $('#trackDetails').html(html);
    }
};