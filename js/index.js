/**
 * Created by Rohan on 4/30/2017.
 */
$(document).ready(function() {
    var btnCreateEvent = $("#btnCreateEvent"), btnShowEvents = $("#btnShowEvents"); eventController = new EventController(),
        durationTypeToggle = $("#durationType"), eventDurationWrapper = $("#eventDurationWrapper"), eventType = "Normal";

    durationTypeToggle.bootstrapToggle({
        off: 'Normal',
        on: 'Lightning'
    });
    btnCreateEvent.on("click", function() {
        createEvent(event, eventType, eventController);
    });
    btnShowEvents.on("click", function() {
        eventController.showEvents();
    });
    durationTypeToggle.change(function() {
        eventDurationWrapper.removeClass("hidden");
        eventType = "Normal";
        if($(this).prop('checked')) {
            eventType = "Lightning";
            eventDurationWrapper.addClass("hidden");
        }
    });

});

function createEvent(evt, eventType, controller) {
    var obj = validateForm();
    if(obj.isValid) {
        obj.type = eventType;
        controller.createEvent(obj);
    }
}

function validateForm() {
    var obj = {isValid: true, eventName: $("#eventName").val(), eventDuration: $("#eventDuration").val()},
        eventNameErrMsg = $(".eventNameErrMsg") ,
        durationErrMsg = $(".durationErrMsg");

    eventNameErrMsg.addClass("hidden");
    if(obj.eventName.trim().length <= 0) {
        obj.isValid = false;
        eventNameErrMsg.removeClass("hidden");
    }
    if(!$("#eventDurationWrapper").hasClass("hidden")) {
        durationErrMsg.addClass("hidden");
        if(obj.eventDuration.length > 0) {
            durationErrMsg.text("Please enter the duration between 1 to 60");
            if(obj.eventDuration < 1 || obj.eventDuration > 60) {
                obj.isValid = false;
                durationErrMsg.removeClass("hidden");
            }
        }else{
            obj.isValid = false;
            durationErrMsg.text("Duration can not be empty");
            durationErrMsg.removeClass("hidden");

        }
    }

    return obj;
}